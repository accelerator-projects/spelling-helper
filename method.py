import random
import nltk
import nltk.tokenize
from WordTags import WordTags
#from nltk import CFG
import string
import exampleTTS
from nltk.parse.generate import generate, demo_grammar


def openFile(file):
    output = open(file, 'r')
    text = output.read().replace("\n", " ")
    text = text.replace('"', "")
    return text


def removePunc(words):
    for i in string.punctuation:
        try:
            words.remove(i)
        except ValueError:
            pass
    return words

def testInput(ans_words, words):
    x = ""
    words_copy=words
    words=nltk.word_tokenize(words)
    words = removePunc(words)
    ans_words = removePunc(ans_words)
    incorrect_words = []
    correct_words = []
    tags = nltk.pos_tag(words)
    print("-----------------------------------")
    for i in range(len(ans_words)):
        try:
            if ans_words[i] != words[i]:

                word_lower = words[i]
                word_lower.lower()
                incorrect_words.append([word_lower, tags[i][1]])

                print("WRONG! You wrote:", ans_words[i],
                      ". Should be:", words[i])

            else:
                word_lower = words[i]
                word_lower.lower()
                correct_words.append(word_lower)
        except IndexError:
            pass
    if len(incorrect_words) == 0:
        print("CORRECT!")
    else:
        print(words_copy)
    print("-----------------------------------")
    return incorrect_words, correct_words


text = openFile('Three_Little_Pigs.txt')
sentences = nltk.sent_tokenize(text)
tags = WordTags()
test = True
try:
    tags.create()
except FileNotFoundError:
    pass
for _ in range(2):  # 5
    index = random.randint(0, len(sentences) - 1)
    words = sentences[index]
    exampleTTS.ttsHash(words, 'en')
    ans = input()
    ans_words = nltk.word_tokenize(ans)
    incorrect_words, correct_words = testInput(
        ans_words, words)
    tags.addToLists(incorrect_words)
    tags.removeFromLists(correct_words)
    tags.save()
for _ in range(10):
    if test:
        sentence = tags.sentenceMaker()
        exampleTTS.ttsHash(sentence, 'en')
        ans = input()
        ans_words = nltk.word_tokenize(ans)
        incorrect_words, correct_words = testInput(
            ans_words, sentence)
        tags.addToLists(incorrect_words)
        tags.removeFromLists(correct_words)
        tags.save()
        # test=False
    # else:
    #     index=random.randint(0,len(sentences))
    #     words=sentences[index]
    #     ans=input(words)
    #     ans_words=nltk.word_tokenize(ans)
    #     incorrect_words=testInput(ans_words,nltk.word_tokenize(words))
    #     test=True
