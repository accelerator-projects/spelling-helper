from answers import answers
from Markov import Markov
import ast
from exampleTTS import ttsHash
from nltk.tokenize import WhitespaceTokenizer
from string import punctuation

def openFile(file):
    output = open(file, 'r')
    text = output.read().replace("\n", " ")
    text = text.replace('"', "")
    return text

def makeSearchDict(m):
    mid_dict={}
    for k,v in m.getNextWordFor().items():
        t=tuple(v)
        mid_dict[t]=k
    new_dict={}
    for i,j in mid_dict.items():
        for l in i:
            try:
                new_dict[l.lower()].append(j)
            except (KeyError,ValueError):
                new_dict[l.lower()]=[j]
    return new_dict

def saveSearchDict(d):
    output=open("Search.txt","w")
    output.write(str(d))

def readSearchDict():
    input=open("Search.txt","r").read()
    d=ast.literal_eval(input)
    return d

def changeProb(m,answers_list,d):
    listSearch(m,answers_list.getCorrect(),d)
    listSearch(m,answers_list.getIncorrect(),d)

def removePunc(words):
    for i in range(len(words)):
        for j in punctuation:
            if words[i][-1]==j:
                words[i]=words[i][:-1]
    for i in punctuation:
        while True:
            try:
                words.remove(i)
            except ValueError:
                break
    return words

def listSearch(m,l,d):
    for i in l:
        if i.lower() in d:
            for j in d[i.lower()]:
                for _ in range(l[i]):
                    m.getNextWordFor()[j].append(i)

def testInput(ans_words, words):
    answer=answers()
    try:
        answer.create()
    except FileNotFoundError:
        pass
    words_copy=words
    words=WhitespaceTokenizer().tokenize(words)
    print(words)
    words = removePunc(words)
    print(words)
    ans_words = removePunc(ans_words)
    correct_words={}
    incorrect_words = {}
    if abs(len(ans_words)-len(words))==1:
        potential_mishear=True
    else:
        potential_mishear=False
    print("-----------------------------------")
    j=0
    for i in range(len(ans_words)):
        try:
            if ans_words[i] != words[j]:
                if potential_mishear==True:
                    if ans_words[i+1] == words[j]:
                        j+=2
                        word_lower = words[i]
                        word_lower.lower()
                        try:
                            correct_words[word_lower]+=1
                        except KeyError:
                            correct_words[word_lower]=1
                        continue
                word_lower = words[j]
                word_lower.lower()
                try:
                    incorrect_words[word_lower]+=1
                except KeyError:
                    incorrect_words[word_lower]=1
                print("WRONG! You wrote:", ans_words[i],
                      ". Should be:", words[i])

            else:
                word_lower = words[i]
                word_lower.lower()
                try:
                    correct_words[word_lower]+=1
                except KeyError:
                    correct_words[word_lower]=1
        except IndexError:
            pass
        j+=1
    if len(incorrect_words) == 0:
        print("CORRECT!")
    else:
        print(words_copy)
    print("-----------------------------------")
    answer.addList(correct_words,incorrect_words)
    answer.save()


print("The idea of this is to provide spelling and listening help.")
print("A random sentence will be spoken to you.")
print("Then you need to type in what you've heard.")
print("You will then be provided with all the words you spelt wrong.")
print("If you use the option to play again, the new sentence will use words you spelt wrong more often.")
print("While the words you spelt incorrectly will appear less often.")
print("Hopefully this helps you. Type y/yes if you are ready to start.")
while True:
    ans=input()
    if ans=='y' or ans=='yes':
        break
    else:
        print("Are you sure? Type y/yes if you want to start")
        ans = input()
        break
if ans != "y" and ans !="yes":
    exit()

text = openFile("speeches.txt").split() #"Three_Little_Pigs"
m=Markov(3)
m.train(text)
try:
    d=readSearchDict()
except FileNotFoundError:
    d=makeSearchDict(m)
    saveSearchDict(d)
while True:
    sentence=m.generate(7)
    sentence=sentence[:-1]+"."
    ttsHash(sentence, 'en')
    ans=input()
    testInput(WhitespaceTokenizer().tokenize(ans),sentence)
    q=input("Would you like to go again? (y/n)")
    if q=='y' or q=='yes':
        continue
    else:
        q2=input("Are you sure?")
        if q2=='y' or q2=='yes':
            break
        else:
            continue
