import ast

class answers:
    def __init__(self):
        self.correct={}
        self.incorrect={}
    def addList(self,correct,incorrect):
        def actualAddList(d1,d2):
            for i,j in d2.items():
                try:
                    d1[i]+=j
                except KeyError:
                    d1[i]=j

        actualAddList(self.correct,correct)
        actualAddList(self.incorrect,incorrect)

    def save(self):
        output=open("Skill.txt","w")
        output.write(str(self.correct)+"\n")
        output.write(str(self.incorrect))
        output.close()

    def create(self):
        output=open("Skill.txt","r")
        rows=output.readlines()
        self.correct=ast.literal_eval(rows[0])
        self.incorrect=ast.literal_eval(rows[1])
        output.close()

    def getCorrect(self):
        return self.correct

    def getIncorrect(self):
        return self.incorrect
