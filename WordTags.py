import nltk
from random import randint, choice
from nltk.tag import map_tag
import ast

class WordTags:
    def __init__(self):
        self.word_types_dict = {
            "verbs": ['hits', 'hit', 'walks', 'imprisoned'],
            "nouns_proper": ['Jack', 'Jill', 'Bob'],
            "nouns_common": ['ball', 'dog', 'cat', 'alien'],
            "nouns": ['Jack', 'Jill', 'Bob', 'ball', 'dog', 'cat', 'alien'],
            "pronouns": [],
            "adjectives": ['pretty', 'happy', 'sad', 'ugly'],
            "adverbs": ['happily', 'greatly', 'madly', 'fully'],
            "adpositions": [],
            "conjunctions": [],
            "determiners": ['the', 'a', 'an'],
            "particles": [],
        }

        self.translation_dict = {
            "VERB": 'verbs',
            "NN": 'nouns_common',
            "NNS": 'nouns_common',
            "NNP": 'nouns_proper',
            "NNPS": 'nouns_proper',
            "NOUN": 'nouns',
            "PRON": 'pronouns',
            "ADJ": 'adjectives',
            "ADV": 'adverbs',
            "ADP": 'adpositions',
            "CONJ": 'conjunctions',
            "DET": 'determiners',
            "PRT": 'particles',
        }

    def addToLists(self, incorrect_words):
        def maptag(i):
            return map_tag('en-ptb', 'universal', i[1])

        for i in incorrect_words:
            if i[1] == 'NN' or i[1] == 'NNS':
                self.word_types_dict["nouns_common"].append(i[0])
                self.word_types_dict["nouns"].append(i[0])
            elif i[1] == 'NNP' or i[1] == 'NNPS':
                self.word_types_dict["nouns_proper"].append(i[0])
                self.word_types_dict["nouns"].append(i[0])
            else:
                try:
                    self.word_types_dict[self.translation_dict[maptag(i)]].append(
                        i[0])
                except KeyError:
                    pass

    def removeFromLists(self, correct_words):
        for i in correct_words:
            for j,k in self.word_types_dict.items():
                if i in k:
                    if k.count(i)>1:
                        self.word_types_dict[j].remove(i)
                        #print("hi")

    def save(self):
        def write_helper(word_type):
            output.write(str(self.word_types_dict[word_type]) + "\n")

        output = open("Misspelling.txt", "w")
        for i in self.word_types_dict:
            write_helper(i)
        output.close()

    def create(self):
        input = open("Misspelling.txt", "r")
        rows = input.readlines()
        i = 0
        for j in self.word_types_dict:
            self.word_types_dict[j] = ast.literal_eval(rows[i])
            i += 1
        input.close()

    def subjectVerb(self):
        common_or_proper = choice([0, 1])
        if common_or_proper == 0:
            determiner = choice(self.word_types_dict['determiners'])
            noun = choice(self.word_types_dict['nouns_common'])
            verb = choice(self.word_types_dict['verbs'])
            determiner1=self.anTest(determiner,noun)
            return determiner1.capitalize() + " " + noun + " " + verb + "."
        else:
            noun = choice(self.word_types_dict['nouns_proper'])
            verb = choice(self.word_types_dict['verbs'])
            return noun.capitalize() + " " + verb + "."

    def subjectVerbObject(self):
        common_or_proper = choice([0, 1])

        determiner = choice(self.word_types_dict['determiners'])
        common_noun = choice(self.word_types_dict['nouns_common'])
        proper_noun = choice(self.word_types_dict['nouns_proper'])
        verb = choice(self.word_types_dict['verbs'])
        next_noun = choice(self.word_types_dict['nouns'])
        determiner1=self.anTest(determiner,common_noun)
        determiner2=self.anTest(determiner,next_noun)
        test = False
        if nltk.pos_tag([next_noun, verb])[0][1] == 'NNS' or nltk.pos_tag([next_noun, verb])[0][1] == 'NN':
            test = True
        if common_or_proper == 0:
            if test:
                return determiner1.lower().capitalize() + " " + common_noun.lower() + " " + verb.lower() + " " + determiner2.lower() + " " + next_noun.lower() + "."
            else:
                return determiner1.lower().capitalize() + " " + common_noun.lower() + " " + verb.lower() + " " + next_noun + "."
        else:
            if test:
                return proper_noun.lower().capitalize() + " " + verb.lower() + " " + determiner2.lower() + " " + next_noun.lower() + "."
            else:
                return proper_noun.lower().capitalize() + " " + verb.lower() + " " + next_noun + "."

    def subjectVerbAdjectiveObject(self):
        common_or_proper = choice([0, 1])

        determiner = choice(self.word_types_dict['determiners'])
        common_noun = choice(self.word_types_dict['nouns_common'])
        proper_noun = choice(self.word_types_dict['nouns_proper'])
        verb = choice(self.word_types_dict['verbs'])
        next_noun = choice(self.word_types_dict['nouns'])
        adjective = choice(self.word_types_dict['adjectives'])
        determiner1=self.anTest(determiner,common_noun)
        determiner2=self.anTest(determiner,adjective)

        if nltk.pos_tag([next_noun, verb])[0][1] == 'NNS' or nltk.pos_tag([next_noun, verb])[0][1] == 'NN':
            test = True
        else:
            test = False

        if common_or_proper == 0:
            if test:
                return determiner1.lower().capitalize() + " " + common_noun.lower() + " " + verb.lower() + " " + determiner2.lower() + " " + adjective.lower() + " " + next_noun.lower() + "."
            else:
                return determiner1.lower().capitalize() + " " + common_noun.lower() + " " + verb.lower() + " " + adjective.lower() + " " + next_noun + "."
        else:
            if test:
                return proper_noun.lower().capitalize() + " " + verb.lower() + " " + determiner2.lower() + " " + adjective.lower() + " " + next_noun.lower() + "."
            else:
                return proper_noun.lower().capitalize() + " " + verb.lower() + " " + adjective.lower() + " " + next_noun + "."

    def subjectVerbAdverb(self):
        common_or_proper = choice([0, 1])

        determiner = choice(self.word_types_dict['determiners'])
        common_noun = choice(self.word_types_dict['nouns_common'])
        proper_noun = choice(self.word_types_dict['nouns_proper'])
        verb = choice(self.word_types_dict['verbs'])
        adverb = choice(self.word_types_dict['adverbs'])
        determiner1=self.anTest(determiner,common_noun)

        if common_or_proper == 0:
            return determiner1.lower().capitalize() + " " + common_noun.lower() + " " + verb.lower() + " " + adverb.lower() + "."
        else:
            return proper_noun.lower().capitalize() + " " + verb.lower() + " " + adverb.lower() + "."

    def sentenceMaker(self):
        # list=['self.subjectVerb()','self.subjectVerbAdverb()','self.subjectVerbAdjectiveObject()','self.subjectVerbAdverb()']
        # rand_func=list[randint(0,len(list)-1)]
        # return eval(rand_func)
        return self.subjectVerbAdjectiveObject()

    def anTest(self,determiner,noun):
        determiner_new=""
        if determiner.lower() == 'a' or determiner.lower() =='an':
            if noun[0] in ['a','e','i','o','u']:
                determiner_new='an'
            else:
                determiner_new='a'
        return determiner_new
